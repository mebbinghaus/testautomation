variable "DO_TOKEN" {}

variable "DO_PUBKEY_PLAIN" {}
variable "DO_KEYFINGERPRINT" {}
variable "DO_REGION" {}
variable "DO_SIZE" {}

variable "DATABASE_TEST_JDBC_URL" {}
variable "DATABASE_TEST_USERNAME" {}
variable "DATABASE_TEST_PASSWORD" {}

variable "SPACE_SECRET_KEY" {}
variable "SPACE_ACCESS_KEY" {}