resource "digitalocean_droplet" "worker" {
  image = "ubuntu-16-04-x64"
  name = "test-droplet"
  region = "${var.DO_REGION}"
  size = "${var.DO_SIZE}"
  private_networking = true
  ssh_keys = [
    "${var.DO_KEYFINGERPRINT}"
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = "${file("~/.ssh/id_rsa")}"
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /sources/src"
    ]
  }

  provisioner "file" {
    source = "../src/"
    destination = "/sources/src"
  }

  provisioner "file" {
    source = "../Dockerfile"
    destination = "/sources/Dockerfile"
  }

  provisioner "file" {
    source = "../pom.xml"
    destination = "/sources/pom.xml"
  }

  provisioner "file" {
    source = "../src/main/resources/.s3cfg"
    destination = "/root/.s3cfg"
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 10",
      "sed -i -- 's@DATABASE_JDBC_URL@${var.DATABASE_TEST_JDBC_URL}@g' /sources/src/main/resources/application.properties",
      "sed -i -- 's@DATABASE_USERNAME@${var.DATABASE_TEST_USERNAME}@g' /sources/src/main/resources/application.properties",
      "sed -i -- 's@DATABASE_PASSWORD@${var.DATABASE_TEST_PASSWORD}@g' /sources/src/main/resources/application.properties",
      "sed -i -- 's@SPACE_SECRET_KEY@${var.SPACE_SECRET_KEY}@g' /root/.s3cfg",
      "sed -i -- 's@SPACE_ACCESS_KEY@${var.SPACE_ACCESS_KEY}@g' /root/.s3cfg",
      "apt-get update",
      "apt-get install apt-transport-https ca-certificates curl software-properties-common -y",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"",
      "apt-get update",
      "apt-get install docker-ce -y",
      "usermod -aG docker `whoami`",
      "apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9",
      "apt-add-repository 'deb http://repos.azulsystems.com/ubuntu stable main'",
      "apt-get update",
      "apt install zulu-11 -y",
      "wget http://apache.mirror.digitalpacific.com.au/maven/maven-3/3.6.2/binaries/apache-maven-3.6.2-bin.tar.gz",
      "tar -xzvf apache-maven-3.6.2-bin.tar.gz",
      "update-alternatives --install /usr/bin/mvn maven $(pwd)/apache-maven-3.6.2/bin/mvn 1001",
      "apt-get install python-setuptools -y",
      "wget https://sourceforge.net/projects/s3tools/files/s3cmd/2.0.1/s3cmd-2.0.1.tar.gz",
      "tar xzf s3cmd-2.0.1.tar.gz",
      "cd s3cmd-2.0.1",
      "python setup.py install",
      "cd /sources",
      "mvn com.github.eirslett:frontend-maven-plugin:1.7.6:install-node-and-npm -DnodeVersion=\"v10.16.0\"",
      "mvn clean install -P production -DskipTests",
      "docker build -t guestbooktest:latest .",
      "docker run -d -p 8080:8080 guestbooktest:latest",
      "sleep 10",
      "mvn clean test -P E2E",
      "exit_status=$?",
      "s3cmd put /sources/target/*.flv s3://codinghaus/EndToEndTests/",
      "exit $exit_status"
    ]
  }

}
