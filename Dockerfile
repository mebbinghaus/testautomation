FROM openjdk:11-jdk-oracle
RUN mkdir /run/guestbook
WORKDIR /run/guestbook
ADD target/guestbook-2.0-SNAPSHOT.jar .
EXPOSE 8080
CMD java -jar /run/guestbook/guestbook-2.0-SNAPSHOT.jar