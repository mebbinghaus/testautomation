package de.codinghaus.spring;

import org.springframework.data.repository.CrudRepository;

public interface GuestbookPostRepository extends CrudRepository<GuestbookPost, Long> {
}
