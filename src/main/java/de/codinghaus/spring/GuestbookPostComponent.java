package de.codinghaus.spring;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.BindingValidationStatus;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.data.value.ValueChangeMode;

import java.util.Optional;
import java.util.stream.Collectors;

public class GuestbookPostComponent extends HorizontalLayout {

    private FormLayout postFormLayout = new FormLayout();
    private HorizontalLayout actionsLayout = new HorizontalLayout();
    private GuestbookPostDTO dtoForBinder;

    public GuestbookPostComponent(String id, final GuestbookService guestbookService, final GuestbookPostDTO guestbookPostDTO) {
        setClassName("guestbookPostComponent");
        boolean isAddNewPostingMode = (guestbookPostDTO == null);
        Binder<GuestbookPostDTO> binder = new Binder<>();
        TextField nameField = new TextField();
        if (isAddNewPostingMode) {
            nameField.setId("namefieldNew");
        } else {
            nameField.setId("namefield"+id);
        }
        nameField.setValueChangeMode(ValueChangeMode.EAGER);
        TextField emailField = new TextField();
        if (isAddNewPostingMode) {
            emailField.setId("emailfieldNew");
        } else {
            emailField.setId("emailfield"+id);
        }
        emailField.setValueChangeMode(ValueChangeMode.EAGER);
        TextField websiteField = new TextField();
        websiteField.setValueChangeMode(ValueChangeMode.EAGER);
        if (isAddNewPostingMode) {
            websiteField.setId("websitefieldNew");
        } else {
            websiteField.setId("websitefield"+id);
        }
        TextArea textArea = new TextArea();
        if (isAddNewPostingMode) {
            textArea.setId("textareaNew");
        } else {
            textArea.setId("textarea"+id);
        }
        NativeButton deleteButton = new NativeButton("Delete");
        NativeButton saveButton = new NativeButton("Save");

        binder.forField(nameField)
                .withValidator(new StringLengthValidator("Please enter your name", 1, null))
                .bind(GuestbookPostDTO::getAuthorName, GuestbookPostDTO::setAuthorName);
        binder.forField(emailField)
                .withValidator(new EmailValidator("Incorrect email address"))
                .bind(GuestbookPostDTO::getAuthorEmail, GuestbookPostDTO::setAuthorEmail);
        binder.forField(websiteField)
                .bind(GuestbookPostDTO::getAuthorWebsite, GuestbookPostDTO::setAuthorWebsite);
        binder.forField(textArea)
                .withValidator(new StringLengthValidator("Please enter your message", 1, null))
                .bind(GuestbookPostDTO::getText, GuestbookPostDTO::setText);

        postFormLayout.addFormItem(nameField, "Author");
        postFormLayout.addFormItem(emailField, "E-Mail");
        postFormLayout.addFormItem(websiteField, "Website");
        postFormLayout.addFormItem(textArea, "Text");
        actionsLayout.add(saveButton, deleteButton);
        saveButton.setEnabled(isAddNewPostingMode);
        deleteButton.setEnabled(!isAddNewPostingMode);
        if (isAddNewPostingMode)
            saveButton.setId("save");
        if (isAddNewPostingMode) {
            dtoForBinder = new GuestbookPostDTO();
            binder.readBean(null);
        }
        else {
            dtoForBinder = guestbookPostDTO;
            binder.readBean(dtoForBinder);
        }

        saveButton.addClickListener( event -> {
            if (binder.writeBeanIfValid(dtoForBinder)) {
                guestbookService.save(dtoForBinder);
                UI.getCurrent().getPage().reload();
            } else {
                BinderValidationStatus<GuestbookPostDTO> validate = binder.validate();
                String errorText = validate.getFieldValidationStatuses()
                        .stream().filter(BindingValidationStatus::isError)
                        .map(BindingValidationStatus::getMessage)
                        .map(Optional::get).distinct()
                        .collect(Collectors.joining(", "));
                Notification.show(errorText);
            }
        });

        deleteButton.setId("delete"+id);
        deleteButton.addClickListener( event -> {
            guestbookService.deleteGuestbookPostByDTO(dtoForBinder);
            UI.getCurrent().getPage().reload();
        });

        add(postFormLayout);
        add(actionsLayout);
    }
}
