package de.codinghaus.spring;

public class GuestbookPostDTO {

    private String authorName;
    private String authorEmail;
    private String authorWebsite;
    private String text;

    public GuestbookPostDTO() {
    }

    public GuestbookPostDTO(String authorName, String authorEmail, String authorWebsite, String text) {
        if (authorName == null || authorEmail == null || authorWebsite == null || text == null) {
            throw new RuntimeException("Please fill all required fields.");
        }
        this.authorName = authorName;
        this.authorEmail = authorEmail;
        this.authorWebsite = authorWebsite;
        this.text = text;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public String getAuthorWebsite() {
        return authorWebsite;
    }

    public String getText() {
        return text;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public void setAuthorWebsite(String authorWebsite) {
        this.authorWebsite = authorWebsite;
    }

    public void setText(String text) {
        this.text = text;
    }
}
