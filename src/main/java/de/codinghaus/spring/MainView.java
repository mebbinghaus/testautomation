package de.codinghaus.spring;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;

@Route("")
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
public class MainView extends VerticalLayout {

    public MainView(@Autowired GuestbookService guestbookService) {
        GuestbookPostComponent newGuestbookPostComponent = new GuestbookPostComponent(null, guestbookService, null);
        Iterable<GuestbookPost> all = guestbookService.findAll();
        add(newGuestbookPostComponent);
        for (GuestbookPost guestbookPost : all) {
            GuestbookPostComponent guestbookPostComponent = new GuestbookPostComponent(guestbookPost.getId().toString(), guestbookService, guestbookService.createGuestbookPostDTO(guestbookPost.getAuthorName(), guestbookPost.getAuthorEmail(), guestbookPost.getAuthorWebsite(), guestbookPost.getText()));
            add(guestbookPostComponent);
        }
    }

}
