package de.codinghaus.spring;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GuestbookPost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String authorName;
    private String authorEmail;
    private String authorWebsite;
    private String text;

    public GuestbookPost() {
    }

    public GuestbookPost(GuestbookPostDTO guestbookPostDTO) {
        setAuthorEmail(guestbookPostDTO.getAuthorEmail());
        setAuthorWebsite(guestbookPostDTO.getAuthorWebsite());
        setAuthorName(guestbookPostDTO.getAuthorName());
        setText(guestbookPostDTO.getText());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getAuthorWebsite() {
        return authorWebsite;
    }

    public void setAuthorWebsite(String authorWebsite) {
        this.authorWebsite = authorWebsite;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
