package de.codinghaus.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class GuestbookService {

    private final GuestbookPostRepository guestbookPostRepository;

    public GuestbookService(@Autowired final GuestbookPostRepository guestbookPostRepository) {
        this.guestbookPostRepository = guestbookPostRepository;
    }

    public GuestbookPostDTO createGuestbookPostDTO(String authorName, String authorEmail, String authorWebsite, String text) {
        return new GuestbookPostDTO(authorName, authorEmail, authorWebsite, text);
    }

    public int getGuestbookPostCount() {
        final ArrayList<GuestbookPost> posts = new ArrayList<>();
        guestbookPostRepository.findAll().forEach(posts::add);
        return posts.size();
    }

    public void save(final GuestbookPostDTO guestbookPostDTO) {
        final GuestbookPost guestbookPost = new GuestbookPost(guestbookPostDTO);
        guestbookPostRepository.save(guestbookPost);
    }

    public Iterable<GuestbookPost> findAll() {
        return guestbookPostRepository.findAll();
    }

    public void deleteGuestbookPostByDTO(GuestbookPostDTO dtoForBinder) {
        Iterable<GuestbookPost> all = guestbookPostRepository.findAll();
        all.forEach(guestbookPost -> {
            if (guestbookPost.getAuthorEmail().equals(dtoForBinder.getAuthorEmail()) &&
                guestbookPost.getAuthorName().equals(dtoForBinder.getAuthorName()) &&
                guestbookPost.getAuthorWebsite().equals(dtoForBinder.getAuthorWebsite()) &&
                guestbookPost.getText().equals(dtoForBinder.getText())) {
                guestbookPostRepository.deleteById(guestbookPost.getId());
            }
        });
    }
}
