package de.codinghaus.spring;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.MySQLContainer;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@ContextConfiguration(initializers = {GuestbookServiceIT.Initializer.class})
public class GuestbookServiceIT {

    @ClassRule
    public static MySQLContainer mysql = new MySQLContainer().withDatabaseName("guestbook").withUsername("test").withPassword("test");

    @Autowired
    private GuestbookService guestbookService;

    @Test
    public void guestbookPostIsCreated() {
        System.out.println(mysql.getJdbcUrl());
        final String AUTHOR_NAME = "Marco Ebbinghaus";
        final String AUTHOR_EMAIL = "ebbinghaus.marco@gmail.com";
        final String AUTHOR_WEBSITE = "http://codinghaus.de";
        final String TEXT = "I was here! Greetings, Marco.";
        int countBefore = guestbookService.getGuestbookPostCount();
        final GuestbookPostDTO guestbookPostDTO = guestbookService.createGuestbookPostDTO(AUTHOR_NAME, AUTHOR_EMAIL, AUTHOR_WEBSITE, TEXT);
        guestbookService.save(guestbookPostDTO);
        int countAfter = guestbookService.getGuestbookPostCount();
        assertEquals(0, countBefore);
        assertEquals(1, countAfter);
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + mysql.getJdbcUrl(),
                    "spring.datasource.username=" + mysql.getUsername(),
                    "spring.datasource.password=" + mysql.getPassword(),
                    "spring.jpa.hibernate.ddl-auto=create"
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }


}
