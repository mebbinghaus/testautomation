package de.codinghaus.spring;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;

public class GuestbookServiceTest {

    @Test
    public void guestbookPostIsCreated() {
        final String AUTHOR_NAME = "Marco Ebbinghaus";
        final String AUTHOR_EMAIL = "ebbinghaus.marco@gmail.com";
        final String AUTHOR_WEBSITE = "http://codinghaus.de";
        final String TEXT = "I was here! Greetings, Marco.";
        final GuestbookPostRepository guestbookPostRepository = mock(GuestbookPostRepository.class);
        final GuestbookService guestbookService = new GuestbookService(guestbookPostRepository);
        final GuestbookPostDTO guestbookPostDTO = guestbookService.createGuestbookPostDTO(AUTHOR_NAME, AUTHOR_EMAIL, AUTHOR_WEBSITE, TEXT);
        assertEquals(guestbookPostDTO.getAuthorName(), AUTHOR_NAME);
        assertEquals(guestbookPostDTO.getAuthorEmail(), AUTHOR_EMAIL);
        assertEquals(guestbookPostDTO.getAuthorWebsite(), AUTHOR_WEBSITE);
        assertEquals(guestbookPostDTO.getText(), TEXT);
    }

    @Test(expected = RuntimeException.class)
    public void incompleteGuestbookPostIsNotCreated() {
        final String AUTHOR_NAME = null;
        final String AUTHOR_EMAIL = "ebbinghaus.marco@gmail.com";
        final String AUTHOR_WEBSITE = "http://codinghaus.de";
        final String TEXT = "I was here! Greetings, Marco.";
        final GuestbookPostRepository guestbookPostRepository = mock(GuestbookPostRepository.class);
        final GuestbookService guestbookService = new GuestbookService(guestbookPostRepository);
        final GuestbookPostDTO guestbookPostDTO = guestbookService.createGuestbookPostDTO(AUTHOR_NAME, AUTHOR_EMAIL, AUTHOR_WEBSITE, TEXT);
        assertEquals(guestbookPostDTO.getAuthorName(), AUTHOR_NAME);
        assertEquals(guestbookPostDTO.getAuthorEmail(), AUTHOR_EMAIL);
        assertEquals(guestbookPostDTO.getAuthorWebsite(), AUTHOR_WEBSITE);
        assertEquals(guestbookPostDTO.getText(), TEXT);
    }

}
