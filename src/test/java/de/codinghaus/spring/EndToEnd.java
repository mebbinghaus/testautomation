package de.codinghaus.spring;

import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.shaded.org.apache.commons.lang.SystemUtils;

import java.io.File;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.testcontainers.containers.BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL;

public class EndToEnd {

    @Rule
    public BrowserWebDriverContainer chrome = new BrowserWebDriverContainer<>()
            .withCapabilities(new ChromeOptions())
            .withRecordingMode(RECORD_ALL, new File("target"));


    @Test
    public void endToEndTest() throws InterruptedException {
        RemoteWebDriver driver = chrome.getWebDriver();
        driver.get("http://"+ (SystemUtils.IS_OS_LINUX ? "172.17.0.1" : "host.docker.internal")+":8080");

        List<WebElement> guestbookPostComponentsInitial = driver.findElementsByClassName("guestbookPostComponent");
        Thread.sleep(5000);
        WebElement namefieldNew = driver.findElementById("namefieldNew");
        WebElement emailfieldNew = driver.findElementById("emailfieldNew");
        WebElement websitefieldNew = driver.findElementById("websitefieldNew");
        WebElement textareaNew = driver.findElementById("textareaNew");
        WebElement saveButton = driver.findElementById("save");
        namefieldNew.sendKeys("Testname");
        emailfieldNew.sendKeys("dasisteintest1337@web.de");
        websitefieldNew.sendKeys("http://www.codinghaus.de");
        textareaNew.sendKeys("Ich war hier! Funktioniert!");
        saveButton.click();

        Thread.sleep(5000);
        List<WebElement> guestbookPostComponentsAfterSave = driver.findElementsByClassName("guestbookPostComponent");

        WebElement deleteButtonOfSavedGuestbookPost = driver.findElementById("delete1");
        deleteButtonOfSavedGuestbookPost.click();

        Thread.sleep(5000);

        List<WebElement> guestbookPostComponentsAfterDelete = driver.findElementsByClassName("guestbookPostComponent");

        assertTrue("0 saved guestbook posts on first page load.", guestbookPostComponentsInitial.size() == 1);
        assertTrue("1 saved guestbook posts after saving.", guestbookPostComponentsAfterSave.size() == 2);
        assertTrue("0 saved guestbook posts after deleting.", guestbookPostComponentsAfterDelete.size() == 1);
    }

}
