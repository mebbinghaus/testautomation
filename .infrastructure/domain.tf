resource "digitalocean_domain" "domain-www" {
  name       = "www.gotcha-app.de"
  ip_address = "${digitalocean_droplet.worker.ipv4_address}"
  depends_on = ["digitalocean_droplet.worker"]
}