resource "digitalocean_droplet" "worker" {
  image = "ubuntu-16-04-x64"
  name = "prod-droplet"
  region = "${var.DO_REGION}"
  size = "${var.DO_SIZE}"
  private_networking = true
  ssh_keys = [
    "${var.DO_KEYFINGERPRINT}"
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = "${file("~/.ssh/id_rsa")}"
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /sources/src"
    ]
  }

  provisioner "file" {
    source = "../src/"
    destination = "/sources/src"
  }

  provisioner "file" {
    source = "../Dockerfile"
    destination = "/sources/Dockerfile"
  }

  provisioner "file" {
    source = "../pom.xml"
    destination = "/sources/pom.xml"
  }

  provisioner "remote-exec" {
    inline = [
      "sleep 10",
      "sed -i -- 's@DATABASE_JDBC_URL@${var.DATABASE_JDBC_URL}@g' /sources/src/main/resources/application.properties",
      "sed -i -- 's@DATABASE_USERNAME@${var.DATABASE_USERNAME}@g' /sources/src/main/resources/application.properties",
      "sed -i -- 's@DATABASE_PASSWORD@${var.DATABASE_PASSWORD}@g' /sources/src/main/resources/application.properties",
      "apt-get update",
      "apt-get install apt-transport-https ca-certificates curl software-properties-common -y",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"",
      "apt-get update",
      "apt-get install docker-ce -y",
      "usermod -aG docker `whoami`",
      "apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9",
      "apt-add-repository 'deb http://repos.azulsystems.com/ubuntu stable main'",
      "apt-get update",
      "apt install zulu-11 -y",
      "wget http://apache.mirror.digitalpacific.com.au/maven/maven-3/3.6.2/binaries/apache-maven-3.6.2-bin.tar.gz",
      "tar -xzvf apache-maven-3.6.2-bin.tar.gz",
      "update-alternatives --install /usr/bin/mvn maven $(pwd)/apache-maven-3.6.2/bin/mvn 1001",
      "cd /sources",
      "mvn com.github.eirslett:frontend-maven-plugin:1.7.6:install-node-and-npm -DnodeVersion=\"v10.16.0\"",
      "mvn clean install -P production -DskipTests",
      "docker build -t guestbooktest:latest .",
      "docker run -d -p 8080:8080 guestbooktest:latest",
    ]
  }

}
