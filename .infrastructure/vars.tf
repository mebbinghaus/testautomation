variable "DO_TOKEN" {}

variable "DO_PUBKEY_PLAIN" {}
variable "DO_KEYFINGERPRINT" {}
variable "DO_REGION" {}
variable "DO_SIZE" {}

variable "DATABASE_JDBC_URL" {}
variable "DATABASE_USERNAME" {}
variable "DATABASE_PASSWORD" {}